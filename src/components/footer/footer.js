import React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import './footer.css'

function Footer() {
    return (<>
        <section className='footer'>
            <article className='footer__item'>
                <Card sx={{ minWidth: 275 , background: '#ECEFF7', boxShadow: 'none', textAlign: 'center'}}>
                    <CardContent sx={{paddingLeft: '0px'}}>
                        <Typography variant="h3" component="div" sx={{fontWeight:'700'}}>
                            Ready to get started? 
                        </Typography>
                        <Typography sx={{ mb: 1.5, margin: '40px 0 40px 0'}} color="text.secondary">
                        Take control of your Business Data. Try saas to make your business better
                        </Typography>
                    </CardContent>
                    <CardActions sx={{paddingLeft: '0px', display: 'flex', justifyContent: 'center'}}>
                        <Button variant="contained" sx={{textTransform: 'none'}}>Get Started</Button>
                    </CardActions>
                </Card>
            </article>
        </section>
        <div className='copyright'>
                <p className='copyright-text'>© BrandName. All rights reserved.</p>
        </div>
    </>)
}

export default Footer