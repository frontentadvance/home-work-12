import React from 'react';
import './main.css'
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import ShieldIcon from '@mui/icons-material/Shield';
import TelegramIcon from '@mui/icons-material/Telegram';
import LocalOfferIcon from '@mui/icons-material/LocalOffer';
import ShareIcon from '@mui/icons-material/Share';
import StorageIcon from '@mui/icons-material/Storage';
import WorkIcon from '@mui/icons-material/Work';
import KeyIcon from '@mui/icons-material/Key';
import InboxIcon from '@mui/icons-material/Inbox';
import CreditCardIcon from '@mui/icons-material/CreditCard';
import Stat from '../img/stat.png'

function Main() {
    return (<>
        <section className='menu'>
            <section className='menu__row-1 row-1'>
                <article className='row-1__column-1'>
                <Card sx={{ minWidth: 275 , background: '#F3F6FA', boxShadow: 'none'}}>
                    <CardContent sx={{paddingLeft: '0px'}}>
                        <Typography variant="h2" component="div" sx={{fontWeight:'700'}}>
                            We help you find the best solution 
                        </Typography>
                        <Typography sx={{ mb: 1.5, margin: '40px 0 40px 0'}} color="text.secondary">
                            Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Ex duo eripuit mentitum.
                        </Typography>
                    </CardContent>
                    <CardActions sx={{paddingLeft: '0px'}}>
                        <Button variant="contained" sx={{textTransform: 'none'}}>Start free trial</Button>
                    </CardActions>
                </Card>
                </article>
                <article className='row-1__column-2'>
                    <div className='window'>
                        <div className='window-top'>
                            <div className='circle'></div>
                            <div className='circle'></div>
                            <div className='circle'></div>
                        </div>
                    </div>
                </article>
            </section>
            <section className='menu__row-2 row-2'>
                <article className='row-2__title'>
                    <Typography variant="h2" component="div" sx={{fontWeight:'700', fontSize: '32px', margin: '120px 0 50px 0', width: '360px', textAlign: 'center'}}>
                        We offer a complete range of features 
                    </Typography>
                </article>
                <article className='row-2__card'>
                    <Card sx={{ maxWidth: '30%', boxShadow: 'none'}}>
                    <Typography sx={{display: 'flex', justifyContent: 'center'}}><ShoppingCartIcon sx = {{color: '#3C64B1', fontSize: '36px'}}/></Typography>
                        <CardContent sx={{paddingLeft: '0px'}}>
                            <Typography variant="h3" component="div" sx={{fontWeight:'500', textAlign: 'center', fontSize: '1.5rem'}}>
                                Store Integration 
                            </Typography>
                            <Typography sx={{ mb: 1.5, margin: '40px 0 40px 0', textAlign: 'center'}} color="text.secondary">
                                Eos tota dicunt democritum no. Has natum gubergren ne. Est viris soleat sadipscing cu. Legere epicuri insolens eu nec, dicit virtute urbanitas id nam, qui in habeo semper eligendi.
                            </Typography>
                        </CardContent>
                    </Card>
                    <Card sx={{ maxWidth: '30%', boxShadow: 'none'}}>
                    <Typography sx={{display: 'flex', justifyContent: 'center'}}><ShieldIcon sx = {{color: '#3C64B1', fontSize: '36px'}}/></Typography>
                        <CardContent sx={{paddingLeft: '0px'}}>
                            <Typography variant="h3" component="div" sx={{fontWeight:'500', textAlign: 'center', fontSize: '1.5rem'}}>
                                Data Protection 
                            </Typography>
                            <Typography sx={{ mb: 1.5, margin: '40px 0 40px 0', textAlign: 'center'}} color="text.secondary">
                                Ne nam phaedrum consequat, adhuc aliquid ea pri, eum eligendi incorrupte referrentur in. Vix ad senserit salutandi argumentum. Ei eam definiebas reformidans, exerci persecuti no ius.
                            </Typography>
                        </CardContent>
                    </Card>
                    <Card sx={{ maxWidth: '30%', boxShadow: 'none'}}>
                    <Typography sx={{display: 'flex', justifyContent: 'center'}}><LocalOfferIcon sx = {{color: '#3C64B1', fontSize: '36px'}}/></Typography>
                        <CardContent sx={{paddingLeft: '0px'}}>
                            <Typography variant="h3" component="div" sx={{fontWeight:'500', textAlign: 'center', fontSize: '1.5rem'}}>
                                Invoice Generator 
                            </Typography>
                            <Typography sx={{ mb: 1.5, margin: '40px 0 40px 0', textAlign: 'center'}} color="text.secondary">
                                Assum suavitate ea vel, vero erat doming cu cum. Zril ornatus sea cu. Pro ex integre pertinax. Cu cum harum paulo legendos, mei et quod enim mnesarchum, habeo affert laoreet sea ei.
                            </Typography>
                        </CardContent>
                    </Card>
                    <Card sx={{ maxWidth: '30%', boxShadow: 'none'}}>
                    <Typography sx={{display: 'flex', justifyContent: 'center'}}><TelegramIcon sx = {{color: '#3C64B1', fontSize: '36px'}}/></Typography>
                        <CardContent sx={{paddingLeft: '0px'}}>
                            <Typography variant="h3" component="div" sx={{fontWeight:'500', textAlign: 'center', fontSize: '1.5rem'}}>
                                Auto Responder 
                            </Typography>
                            <Typography sx={{ mb: 1.5, margin: '40px 0 40px 0', textAlign: 'center'}} color="text.secondary">
                                Assum suavitate ea vel, vero erat doming cu cum. Zril ornatus sea cu. Pro ex integre pertinax. Cu cum harum paulo legendos, mei et quod enim mnesarchum, habeo affert laoreet sea ei.
                            </Typography>
                        </CardContent>
                    </Card>
                    <Card sx={{ maxWidth: '30%', boxShadow: 'none'}}>
                    <Typography sx={{display: 'flex', justifyContent: 'center'}}><ShareIcon sx = {{color: '#3C64B1', fontSize: '36px'}}/></Typography>
                        <CardContent sx={{paddingLeft: '0px'}}>
                            <Typography variant="h3" component="div" sx={{fontWeight:'500', textAlign: 'center', fontSize: '1.5rem'}}>
                                Social Plugins 
                            </Typography>
                            <Typography sx={{ mb: 1.5, margin: '40px 0 40px 0', textAlign: 'center'}} color="text.secondary">
                                Eos tota dicunt democritum no. Has natum gubergren ne. Est viris soleat sadipscing cu. Legere epicuri insolens eu nec, dicit virtute urbanitas id nam, qui in habeo semper eligendi.
                            </Typography>
                        </CardContent>
                    </Card>
                    <Card sx={{ maxWidth: '30%', boxShadow: 'none'}}>
                    <Typography sx={{display: 'flex', justifyContent: 'center'}}><StorageIcon sx = {{color: '#3C64B1', fontSize: '36px'}}/></Typography>
                        <CardContent sx={{paddingLeft: '0px'}}>
                            <Typography variant="h3" component="div" sx={{fontWeight:'500', textAlign: 'center', fontSize: '1.5rem'}}>
                                Data Backup 
                            </Typography>
                            <Typography sx={{ mb: 1.5, margin: '40px 0 40px 0', textAlign: 'center'}} color="text.secondary">
                                Ne nam phaedrum consequat, adhuc aliquid ea pri, eum eligendi incorrupte referrentur in. Vix ad senserit salutandi argumentum. Ei eam definiebas reformidans, exerci persecuti no ius.
                            </Typography>
                        </CardContent>
                    </Card>
                </article>
            </section>
            <section className='menu__row-3 row-3'>
                <article className='row-3__column-1'>
                    <Card sx={{ minWidth: 275 , background: 'white', boxShadow: 'none'}}>
                        <CardContent sx={{paddingLeft: '0px'}}>
                            <Typography variant="h3" component="div" sx={{fontWeight:'700'}}>
                                Product Statistic 
                            </Typography>
                            <Typography sx={{ mb: 1.5, margin: '40px 0 40px 0'}} color="text.secondary">
                                Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum. At nam minimum ponderum. Est audiam animal molestiae te. Ex duo eripuit mentitum.
                            </Typography>
                        </CardContent>
                        <CardActions sx={{paddingLeft: '0px'}}>
                            <Button variant="contained" sx={{textTransform: 'none'}}>Learn more</Button>
                        </CardActions>
                    </Card>
                </article>
                <article className='row-3__column-2'>
                    <img src={Stat} alt = 'statisticks' className='img-stat'/>
                </article>
            </section>
            <section className='menu__row-4 row-4'>
                <article className='row-4__column-1'>
                    <div className='window'>
                            <div className='window-top'>
                                <div className='circle'></div>
                                <div className='circle'></div>
                                <div className='circle'></div>
                            </div>
                        </div>
                </article>
                <article className='row-4__column-2'>
                    <Card sx={{ maxWidth: '50%', boxShadow: 'none'}}>
                        <Typography sx={{display: 'flex', justifyContent: 'flex-start'}}><WorkIcon sx = {{color: '#3C64B1', fontSize: '26px'}}/></Typography>
                        <CardContent sx={{paddingLeft: '0px'}}>
                            <Typography sx={{ mb: 1.5, margin: '0 0 0 0', textAlign: 'left'}} color="text.secondary">
                                Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.
                            </Typography>
                        </CardContent>
                    </Card>
                    <Card sx={{ maxWidth: '50%', boxShadow: 'none'}}>
                        <Typography sx={{display: 'flex', justifyContent: 'flex-start'}}><KeyIcon sx = {{color: '#3C64B1', fontSize: '26px'}}/></Typography>
                        <CardContent sx={{paddingLeft: '0px'}}>
                            <Typography sx={{ mb: 1.5, margin: '0 0 0 0', textAlign: 'left'}} color="text.secondary">
                                At nam minimum ponderum. Est audiam animal molestiae te. Ex duo eripuit mentitum.
                            </Typography>
                        </CardContent>
                    </Card>
                    <Card sx={{ maxWidth: '50%', boxShadow: 'none'}}>
                        <Typography sx={{display: 'flex', justifyContent: 'flex-start'}}><InboxIcon sx = {{color: '#3C64B1', fontSize: '26px'}}/></Typography>
                        <CardContent sx={{paddingLeft: '0px'}}>
                            <Typography sx={{ mb: 1.5, margin: '0 0 0 0', textAlign: 'left'}} color="text.secondary">
                                Vidisse oblique ei has, eos eu atomorum erroribus cotidieque, mazim postea ne vel.
                            </Typography>
                        </CardContent>
                    </Card>
                    <Card sx={{ maxWidth: '50%', boxShadow: 'none'}}>
                        <Typography sx={{display: 'flex', justifyContent: 'flex-start'}}><CreditCardIcon sx = {{color: '#3C64B1', fontSize: '26px'}}/></Typography>
                        <CardContent sx={{paddingLeft: '0px'}}>
                            <Typography sx={{ mb: 1.5, margin: '0 0 0 0', textAlign: 'left'}} color="text.secondary">
                                Ea aperiri ponderum ullamcorper sit, verear offendit imperdiet nam eu, liber delicata tractatos ad.
                            </Typography>
                        </CardContent>
                    </Card>
                </article>           
            </section>
        </section>
    </>)
}

export default Main
