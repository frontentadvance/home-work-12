import React from 'react';
// import { useState } from 'react';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import Button from '@mui/material/Button';
import './header.css'
    

function Header() {

    const [value, setValue] = React.useState('1');

    const handleChange = (event, newValue) => {
    setValue(newValue);
    };

    return (<>
        <section className='header'>
            <article className='header__logo'>
            <p className='logo'>LOGO</p>
            </article>
            <article className='header__menu'>
            <Box sx={{ width: '100%', typography: 'body1'}}>
                <TabContext value={value}>
                    <Box>
                    <TabList sx={{}} onChange={handleChange} aria-label="lab API tabs example">
                        <Tab sx={{textTransform: 'none', fontSize: '14px'}} label="Home" value="1"/>
                        <Tab sx={{textTransform: 'none', fontSize: '14px'}} label="Features" value="2" />
                        <Tab sx={{textTransform: 'none', fontSize: '14px'}} label="Process" value="3" />
                        <Tab sx={{textTransform: 'none', fontSize: '14px'}} label="Discover" value="4" />
                        <Tab sx={{textTransform: 'none', fontSize: '14px'}} label="Feedback" value="5" />
                    </TabList>
                    </Box>
                    {/* <TabPanel value="1">Item One</TabPanel>
                    <TabPanel value="2">Item Two</TabPanel>
                    <TabPanel value="3">Item Three</TabPanel> */}
                </TabContext>
            </Box>
            </article>
            <article className='header__button'>
                <Button sx={{textTransform: 'none'}} variant="contained">Log In</Button>
            </article>

        </section>
    </>)
}

export default Header